#!/bin/bash
. /.envfile

MONGODB_HOST=${MONGODB_RESTORE_PORT_27017_TCP_ADDR:-${MONGODB_RESTORE_HOST}}
MONGODB_HOST=${MONGODB_RESTORE_PORT_1_27017_TCP_ADDR:-${MONGODB_RESTORE_HOST}}
MONGODB_PORT=${MONGODB_RESTORE_PORT_27017_TCP_PORT:-${MONGODB_RESTORE_PORT}}
MONGODB_PORT=${MONGODB_RESTORE_PORT_1_27017_TCP_PORT:-${MONGODB_RESTORE_PORT}}
MONGODB_USER=${MONGODB_RESTORE_USER:-${MONGODB_RESTORE_ENV_MONGODB_USER}}
MONGODB_PASS=${MONGODB_RESTORE_PASS:-${MONGODB_RESTORE_ENV_MONGODB_PASS}}

[[ ( -z "${MONGODB_RESTORE_USER}" ) && ( -n "${MONGODB_RESTORE_PASS}" ) ]] && MONGODB_RESTORE_USER='dbroot'

[[ ( -n "${MONGODB_RESTORE_USER}" ) ]] && USER_RESTORE_STR+=" --username ${MONGODB_RESTORE_USER}"
[[ ( -n "${MONGODB_RESTORE_PASS}" ) ]] && USER_RESTORE_STR+=" --password ${MONGODB_RESTORE_PASS}"
[[ ( -n "${MONGODB_RESTORE_DB}" ) ]] && USER_RESTORE_STR+=" --db ${MONGODB_RESTORE_DB}"
[[ ( -n "${MONGODB_RESTORE_AUTHDB}" ) ]] && USER_RESTORE_STR+=" --authenticationDatabase ${MONGODB_RESTORE_AUTHDB}"

FILE_TO_RESTORE=${1}

[[ -z "${1}" ]] && FILE_TO_RESTORE=$(ls /backup -N1 | grep -iv ".tgz" | sort -r | head -n 1)

echo "=> Restore database mongorestore --host ${MONGODB_RESTORE_HOST} --port ${MONGODB_RESTORE_PORT} ${USER_RESTORE_STR} ${EXTRA_RESTORE_OPTS} /backup/$FILE_TO_RESTORE/${MONGODB_RESTORE_DB}"

RESTORE_CMD="mongorestore --host ${MONGODB_HOST} --port ${MONGODB_PORT} ${USER_RESTORE_STR} ${EXTRA_RESTORE_OPTS} /backup/$FILE_TO_RESTORE/${MONGODB_RESTORE_SRC_DB}"


RESTORE_RESULTS=${RESTORE_CMD}
if ${RESTORE_RESULTS}; then
    echo "   Restore succeeded"
else
    echo "   Restore failed"
fi
echo "=> Done"
